## What Is This?
Create a custom coco dataset for Mask RCNN model 

## Dependencies

1. [labelme](https://github.com/wkentaro/labelme)
2. imgaug
 >conda install imgaug  or
 >pip install imgaug

3. python 3.6

## Create Dataset

1. **Collect** around 16-20 photos of your training items.

>It's better to take the photo in different background

2. **Change** the followings line in `augment_images.py` to wherever your image directory. If the images_multiplier is set to **5**,

>100 images will be split into test train according to your train ratio.

3. **Change** the augmentation settings in `augment_images.py` according to your preferences.
`Do note that only geometry augmentation will be applied to val images.`

```py
directory = './raw_images/'
images_multiplier=5
train_ratio=0.8
output_dir='./augmented_images/' #output dir will be created automatically
```
>python augment_images.py

This will create the following directory with the following structure.
```txt
augmented_images
|_train
	|_.jpg images
|_val
	|_.jpgs images
```

> Now you have 70 train images, 40 val images.

## Label Dataset

1. **Run** labelme

>python labelme

2. **Label** all the images in both directory, [train]() and [val]() in the generated `augmented_images/` directory. Please follow steps under **Create Dataset** to create said directory.

3. **Change** the classes in the `labels.txt` according to your class.
`Keep the __ignore__ and background.`

```txt
__ignore__
_background_
custom_class1
custom_class2
```

## Prepare Training Data

1. **Run** `labelme2coco.py`

> python labelme2coco.py --labels labels.txt unimate_train train train_data
> python labelme2coco.py --labels labels.txt unimate_val val_data

2. **Ensure** that your dataset folder structure should have the following structure.

```txt
dataset_name
|_train
	|_JPEGImages [Contains all the images for training]
	|_annotations.json
|_val
	|_JPEGImages [Contains all the images for training]
	|_annotations.json
```

## Now you're ready to train your dataset
