#!/usr/bin/python3

import imgaug as ia
from imgaug import augmenters as iaa
import glob
import os
import cv2
import random
ia.seed(1)

directory = './raw_images/'
images_multiplier=5
train_ratio=0.8
output_dir='./augmented_images/'


#check if directory is created
if not os.path.exists(output_dir):
   os.makedirs(output_dir+"train/")
   os.makedirs(output_dir+"val/")
images= os.listdir(directory)
image_list= [ os.path.join(directory,image) for image in images if os.path.isfile(directory+image)]
#image_list= os.path.join(directory,images)
print(image_list)
list_size = len(image_list)

count=0
for num in range(images_multiplier):
	random.shuffle(image_list)
	train_images = image_list[:int(train_ratio*list_size)]
	for image_ in image_list:
		#print(image_)
		train=False
		if(image_ in train_images):
			train=True
		image=cv2.imread(image_)
		#cv2.imshow("hello",image)
		#cv2.waitKey(0)
		if(train):
			seq = iaa.Sequential([
				iaa.Flipud(0.2),
				iaa.Fliplr(0.5), # horizontal flips
				iaa.Crop(percent=(0, 0.1)), # random crops
				# Small gaussian blur with random sigma between 0 and 0.5.
				# But we only blur about 50% of all images.
				iaa.Sometimes(
				0.5,
				iaa.GaussianBlur(sigma=(0, 0.5))
				),
				# Strengthen or weaken the contrast in each image.
				#iaa.LinearContrast((0.75, 1.5)),
				# Add gaussian noise.
				# For 50% of all images, we sample the noise once per pixel.
				# For the other 50% of all images, we sample the noise per pixel AND
				# channel. This can change the color (not only brightness) of the
				# pixels.
				iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
				iaa.Sometimes(
				0.5,
				iaa.AddToHueAndSaturation((-30, 30), per_channel=True),
				iaa.MultiplySaturation((0.7, 1.3))
				),
				# Make some images brighter and some darker.
				# In 20% of all cases, we sample the multiplier once per channel,
				# which can end up changing the color of the images.
				iaa.Multiply((0.8, 1.2), per_channel=0.2),
				# Apply affine transformations to each image.
				# Scale/zoom them, translate/move them, rotate them and shear them.
				iaa.Affine(
				scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
				translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
				rotate=(-60, 60),
				shear=(-10, 10)
				)
			], random_order=True) # apply augmenters in random order
		else:
			seq = iaa.Sequential([
				iaa.Flipud(0.2),
				iaa.Fliplr(0.5), # horizontal flips
				iaa.Crop(percent=(0, 0.1)), # random crops
				iaa.Affine(
				scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
				translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
				rotate=(-60, 60),
				shear=(-10, 10)
				)
			], random_order=True) # apply augmenters in random order
		seq_det = seq.to_deterministic()
		image_aug = seq_det.augment_image(image)
		#cv2.imshow("AFTER",image_aug)
		#cv2.waitKey(0)
		if train:
		    new_image_file =output_dir+"train/augment_" + str(count) +".jpg"
		else:
		    new_image_file =output_dir+"val/augment_" + str(count) +".jpg"
		count+=1
		print(new_image_file)
		cv2.imwrite(new_image_file, image_aug,)
